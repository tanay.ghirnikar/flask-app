######################### Flask Imports ###################################
from flask import Flask
from flask import request
from flask import url_for
from flask import redirect
from flask import render_template
from flask import session
###########################################################################

app = Flask(__name__,static_url_path='/static')


@app.route('/',methods=['POST','GET'])
def index():
    if(request.method == 'GET'):
        return render_template('index.html')